# OpenML dataset: arcene

https://www.openml.org/d/1458

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: UCI
**Please cite**:   

ARCENE's task is to distinguish cancer versus normal patterns from mass-spectrometric data. This is a two-class classification problem with continuous input variables. This dataset is one of 5 datasets of the NIPS 2003 feature selection challenge.

Source:
 a. Original owners 
The data were obtained from two sources: The National Cancer Institute (NCI) and the Eastern Virginia Medical School (EVMS). All the data consist of mass-spectra obtained with the SELDI technique. The samples include patients with cancer (ovarian or prostate cancer), and healthy or control patients. 

b. Donor of database 
This version of the database was prepared for the NIPS 2003 variable and feature selection benchmark by Isabelle Guyon, 955 Creston Road, Berkeley, CA 94708, USA (isabelle '@' clopinet.com). 

Data Set Information:
ARCENE was obtained by merging three mass-spectrometry datasets to obtain enough training and test data for a benchmark. The original features indicate the abundance of proteins in human sera having a given mass value. Based on those  features one must separate cancer patients from healthy patients. We added a 
 number of distractor feature called 'probes' having no predictive power. The order  of the features and patterns were randomized. This dataset is one of five datasets used in the NIPS 2003 feature selection challenge.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1458) of an [OpenML dataset](https://www.openml.org/d/1458). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1458/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1458/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1458/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

